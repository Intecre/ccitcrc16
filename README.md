# README #

CCIT Crc16 library in dotNet.

Create a new instance of Crc16Ccitt, pasing the required poly table preset as the argument (enumerated).
Enumerations for preset:
* None - 0x0000;
* NonZero1 - 0xFFFF;
* NonZero2 - 0x1D0F;

Use the overloaded ComputeChecksum to return the crc for your data.  one of the options permits you to provide the previous CRC, useful if you are CRC'ing a group of objects. Run it over the first, use the computed crc on the next, etc until you've finished.

MIT License.