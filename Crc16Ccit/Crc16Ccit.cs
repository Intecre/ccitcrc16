﻿using System;

public enum InitialCrcValue { Zeros, NonZero1 = 0xffff, NonZero2 = 0x1D0F }

/// <summary>
/// Generates CRCs using the CCIT CRC16 method.  creates the Poly Table when created, using specified InitialCrcValue.  
/// </summary>
namespace Crc16Ccit
{
  public class Crc16Ccitt
  {
    // Taken and modified slightly from here: http://www.sanity-free.org/133/crc_16_ccitt_in_csharp.html

    const ushort poly = 4129;
    ushort[] table = new ushort[256];
    ushort initialValue = 0;

    /// <summary>
    /// Compute the check sum of a byte array. Uses InitialCrcValue as crc seed.
    /// </summary>
    /// <param name="bytes"> byte array to CRC</param>
    /// <returns>CRC</returns>
    public ushort ComputeChecksum(byte[] bytes) => ComputeChecksum(bytes, this.initialValue);

    /// <summary>
    /// Compute the check sum of a byte array. Use this overload to seed crc.
    /// </summary>
    /// <param name="bytes"> byte array to CRC</param>
    /// <param name="crcInit">crc initialiser. nb does not affect poly table.</param>
    /// <returns>CRC</returns>
    public ushort ComputeChecksum(byte[] bytes, ushort crcInit) => ComputeChecksum(bytes, 0, crcInit);

    /// <summary>
    /// Compute the check sum of a byte array. Use this overload to specify start position.
    /// </summary>
    /// <param name="bytes">byte array to CRC</param>
    /// <param name="startPos">start position in the array. Throws exception if invald</param>
    /// <returns></returns>
    public ushort ComputeChecksum(byte[] bytes, int startPos) => ComputeChecksum(bytes, startPos, this.initialValue);

    /// <summary>
    /// Compute the check sum of a byte array. Use this overload to specify start position and seed
    /// </summary>
    /// <param name="bytes">byte array to CRC</param>
    /// <param name="startPos">start position in the array. Throws exception if invald</param>
    /// <param name="crcInit">crc initialiser. nb does not affect poly table.</param>
    /// <returns></returns>
    public ushort ComputeChecksum(byte[] bytes, int startPos, ushort crcInit)
    {
      if (startPos < 0 || startPos >= bytes.Length)
        throw new ArgumentOutOfRangeException($"startPos value {startPos} is outside of bytes array bounds.");

      ushort crc = crcInit;
      for (int i = startPos; i < bytes.Length; ++i)
      {
        crc = (ushort)((crc << 8) ^ table[((crc >> 8) ^ (0xff & bytes[i]))]);
      }
      return crc;
    }


    /// <summary>
    /// InitialCrcValue is used to seed the poly table.
    /// </summary>
    /// <param name="initialValue"> For Beckhoff controllers, use NonZero1 (0xFFFF).</param>
    public Crc16Ccitt(InitialCrcValue initialValue)
    {
      this.initialValue = (ushort)initialValue;
      ushort temp, a;
      for (int i = 0; i < table.Length; ++i)
      {
        temp = 0;
        a = (ushort)(i << 8);
        for (int j = 0; j < 8; ++j)
        {
          if (((temp ^ a) & 0x8000) != 0)
          {
            temp = (ushort)((temp << 1) ^ poly);
          }
          else
          {
            temp <<= 1;
          }
          a <<= 1;
        }
        table[i] = temp;
      }
    }
  }
}